function formatName (user) {
	return user.firstName + ' ' + user.lastName;
}

const user  = {
	firstName : 'Juneyoung'
	, lastName : 'Oh'
}

const element = {
	<h1>
		Hello, {formatName(user)}
	</h1>
}

ReactDOM.render(
	element
	, document.elementById('root')
);