import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Game from './vo/game.js'


ReactDOM.render(
  <Game />
  , document.getElementById('root')
);

//ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
