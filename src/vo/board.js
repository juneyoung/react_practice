import React from 'react';
// import Square from './square.js';

// After removing Square class
// This can not be locate inside of the class
function Square (props) {
	return (
		<button className='square' onClick={props.onClick}>
			{props.value}
		</button>
	)
}


export default class Board extends React.Component {



	// Do not forget to surround value of returns 
	renderSquare (i) {
		console.log('render squares', i);

		return  (
			<Square value={this.props.squares[i]} 
				onClick={ () => this.props.onClick(i)} 
			/>
		);
	}


	render () {
		return (
			<div>
				<div className='board-row'>
					{this.renderSquare(0)}
					{this.renderSquare(1)}
					{this.renderSquare(2)}
				</div>

				<div className='board-row'>
					{this.renderSquare(3)}
					{this.renderSquare(4)}
					{this.renderSquare(5)}
				</div>

				<div className='board-row'>
					{this.renderSquare(6)}
					{this.renderSquare(7)}
					{this.renderSquare(8)}
				</div>

			</div>
		);
	}
} 

// export default Board;